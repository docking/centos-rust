# centos-rust

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.4
ARG CENTOS_VERSION=7
ARG RUST_VERSION=1.43.1


FROM ${DOCKER_REGISTRY_URL}centos-rust-mini:${CUSTOM_VERSION}-${CENTOS_VERSION}-${RUST_VERSION} AS centos-rust

WORKDIR /root

RUN \
	yum install -y \
		binutils-devel \
		clang \
		clang-devel \
		cmake \
		curl \
		elfutils-devel \
		gcc \
		gcc-c++ \
		jemalloc-devel \
		jq \
		libpcap-devel \
		llvm \
		make \
		openssl \
		openssl-devel \
		pcre-devel \
		pkgconfig \
		rpm-build \
		rpm-sign \
		unzip \
		xz-devel \
		zip \
		zlib-devel \
	; \
	yum clean all ; \
	rpmdb --rebuilddb

RUN \
	export CARGO_HOME="/usr/local" ; \
	rustup component add \
		clippy \
		rustfmt \
	; \
	cargo install \
		cargo-cache \
		cargo-deb \
	; \
	cargo install --force --git https://github.com/romac/cargo-build-deps ; \
	cargo cache -r all

RUN \
	curl -Lo kcov-36.tar.gz https://github.com/SimonKagstrom/kcov/archive/v36.tar.gz && ( \
		tar zxf kcov-36.tar.gz ; \
		mkdir -p kcov-36/build ; \
		cd kcov-36/build ; \
		cmake .. ; \
		make -j $(nproc) ; \
		make install ; \
	) ; \
	rm -fR kcov-36 kcov-36.tar.gz
