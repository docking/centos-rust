# centos-rust

A Docker image based on CentOS including a Rust toolchain and tools.

Rust tools:
 - clippy
 - rustfmt
 - cargo-build-deps (from https://github.com/romac/cargo-build-deps)
 - cargo-cache
 - cargo-deb

Development tools:
 - build-essential (gcc, g++, make)
 - clang
 - cmake
 - kcov (from https://github.com/SimonKagstrom/kcov)
 - llvm
 - make
 - pkg-config

Development libraries:
 - binutils-devel
 - clang-devel
 - elfutils-devel
 - jemalloc-devel
 - libpcap-devel
 - openssl-devel
 - pcre-devel
 - xz-devel
 - zlib-devel

Misc. tools:
 - openssl
 - rpm-build
 - rpm-sign
 - unzip
 - zip

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the container image: `make build`
* Try the container: `make shell`
